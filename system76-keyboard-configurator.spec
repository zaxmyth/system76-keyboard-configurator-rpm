Name:		system76-keyboard-configurator
Release: 	1%{?dist}
Version: 	1.0.0
Summary: 	system76-keyboard-configurator
License: 	GPLv3
URL: 		https://github.com/pop-os/keyboard-configurator
Source:		system76-keyboard-configurator-%{version}.tar.gz

BuildRequires: cargo 
BuildRequires: libudev-devel
BuildRequires: gtk3-devel

%description
System76 Keyboard Configurator

%define debug_package %{nil}

%prep
%setup -q -n keyboard-configurator-%{version}

%build
make prefix=/usr libdir=%{_libdir}

%install
%make_install prefix=/usr libdir=%{_libdir}

%files
%{_bindir}/system76-keyboard-configurator
%{_libdir}/libsystem76_keyboard_configurator.so
%{_libdir}/pkgconfig/system76_keyboard_configurator.pc
%{_includedir}/system76_keyboard_configurator.h
%{_datadir}/applications/com.system76.keyboardconfigurator.desktop
%{_datadir}/metainfo/com.system76.keyboardconfigurator.appdata.xml
%{_datadir}/icons/hicolor/scalable/apps/com.system76.keyboardconfigurator.svg

%license LICENSE
