# System76 Keyboard Configurator RPM

Package the System76 Keyboard Configurator as an RPM

_[System76 Keyboard Configurator Source](https://github.com/pop-os/keyboard-configurator)_


## Installation

The RPM is published on [COPR](https://copr.fedorainfracloud.org/coprs/zach/system76-keyboard-configurator/).

```
dnf copr enable zach/system76-keyboard-configurator 
dnf install system76-keyboard-configurator
```
